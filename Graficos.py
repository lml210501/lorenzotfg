import pandas as pd
import matplotlib.pyplot as plt

# leer el archivo CSV
datos = pd.read_csv('C:\\Users\\Casa\\TFG\\VinosQualityL.csv')

# crear un gráfico de barras para cada variable
for column in datos.columns[:-1]:
    variable_por_calidad = datos.groupby('quality')[column].mean()
    fig, ax = plt.subplots()
    ax.bar(variable_por_calidad.index, variable_por_calidad.values)
    ax.set_xlabel('Calidad')
    ax.set_ylabel(column)
    ax.set_title(f'Relación entre calidad y {column}')
    plt.show()
