import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score, confusion_matrix

# Cargar los datos
datos = pd.read_csv('C:\\Users\\Casa\\TFG\\VinosQualityL.csv')
# Crear una nueva variable "quality_label" basada en la variable "quality"
datos['quality_label'] = np.where(datos['quality'] <= 6.5, 0, 1)

# Seleccionar las columnas que vamos a utilizar en el modelo
X = datos[['fixed acidity', 'volatile acidity', 'citric acid', 'residual sugar', 'chlorides',
        'free sulfur dioxide', 'total sulfur dioxide', 'density', 'pH', 'sulphates', 'alcohol']]
y = datos['quality_label']

# Normalizar las características
estandar = StandardScaler()
X = estandar.fit_transform(X)

# Dividir los datos en entrenamiento y prueba
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)

# Crear el modelo y entrenarlo
modelo = RandomForestClassifier(n_estimators=100, random_state=42)
modelo.fit(X_train, y_train)

# Hacer las predicciones
prediccion = modelo.predict(X_test)

# Calcular la exactitud y la matriz de confusión
exactitud = accuracy_score(y_test, prediccion)
matriz = confusion_matrix(y_test, prediccion)

# Imprimir los resultados
print("Exactitud:", exactitud)
print("Matriz de confusión:\n", matriz)

# Obtener la importancia de las características
importancia = modelo.feature_importances_
std = np.std([tree.feature_importances_ for tree in modelo.estimators_], axis=0)
indices = np.argsort(importancia)[::-1]

# Imprimir la importancia de las características ordenadas por nombre de columna
print("Importancia de las características:")
for f in range(X.shape[1]):
    print("%d. %s (%f)" % (f + 1, datos.columns[indices[f]], importancia[indices[f]]))
